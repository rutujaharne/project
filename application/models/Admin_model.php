<?php
header('Cache-Control: max-age=900');
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
        	
	}

    function insertEmployee($data)
    {
        $this->db->insert('employee',$data);
        if($this->db->affected_rows() > 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    function delete_employee($data,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('employee', $data);
        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function editEmployee($data,$id)

    {

      $this->db->where('id', $id);

      $this->db->update('employee', $data);

      return TRUE;  

    }

}

?>