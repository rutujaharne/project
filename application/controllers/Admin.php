<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('parser');     
    }

	public function index()
	{
		$this->load->view('login');
	}

	public function employeeList()
	{
		$this->load->view('index');
	}

    public function register()
    {
        $this->load->view('register');
    }

	public function addEmployee(){
           
        if(isset($_POST['submit'])){

        	$created_date = date('Y-m-d h:i:s', time());

            $update_date = date('Y-m-d h:i:s', time());

	        $data = array(

                'name' => $this->input->post('name'),

                'email' => $this->input->post('email'),

                'password' => $this->input->post('password'),

                'status'=>1,

                'created_at' => $created_date,

                'updated_at' => $update_date

	         );
	              
	        $result=$this->Admin_model->insertEmployee($data);

	        if($result==TRUE)
	        {

	            $this->session->set_flashdata('message', 'Record Inserted Successfully');
	            redirect('Admin/employeeList');

	        }else{

	            $this->session->set_flashdata('error', 'Error Insert Record');
	            redirect('Admin/employeeList');

	        }

        }
	    else if(isset($_POST['update'])){
	            
	        $id = $_POST['id'];

	        $update_date = date('Y-m-d h:i:s', time());
	           
	        $data = array(

	        'name' => $this->input->post('name'),

	        'email' => $this->input->post('email'),

	        'password' => $this->input->post('password'),

	        'updated_at' => $update_date

	        );
	        //print_r($data); die;

	        $result=$this->Admin_model->editEmployee($data,$id);
	        
	        if($result==TRUE)
	        {

	            $this->session->set_flashdata('message', 'Record Updated Successfully');

	            redirect('Admin/employeeList');

	        }else{

	            $this->session->set_flashdata('error', 'Error Update Record');

	            redirect('Admin/employeeList');

	        }

	    } 
	}

	public function delete_employee(){

        $employee_id = $_POST['emp_id'];

        $data = array(
        	'status'=>0
        );

        $result = $this->Admin_model->delete_employee($data, $employee_id);

        if($result == TRUE)
        {
            $this->session->set_flashdata('message', 'Record Deleted Successfully');
        }
        else
        {
            $this->session->set_flashdata('error', 'Error Delete Record');
        }

    }

    public function login()  
    {  
        $user = $this->input->post('username');  
        $pass = $this->input->post('password');
        //echo $user.'-'.$pass ; die; 
        if ($user=='admin' && $pass=='admin')   
        {   
            $this->session->set_userdata(array('user'=>$user));  
            $this->load->view('index');  
        }  
        else{  
        	$this->session->set_flashdata('error', 'Your Account is Invalid');
            $this->load->view('login');  
        }  
    }  

    public function logout()  
    {  
        //removing session  
        $this->session->unset_userdata('user');  
        $this->load->view('login'); 
    }  


    public function saveRegister()  
    {  
        if(isset($_POST['submit'])){

            $created_date = date('Y-m-d h:i:s', time());

            $update_date = date('Y-m-d h:i:s', time());

            $data = array(

                'name' => $this->input->post('name'),

                'email' => $this->input->post('email'),

                'password' => $this->input->post('password'),

                'status'=>1,

                'created_at' => $created_date,

                'updated_at' => $update_date

             );
                  
            $result=$this->Admin_model->insertEmployee($data);

            if($result==TRUE)
            {

                $this->session->set_flashdata('message', 'You Registered Successfully');
                redirect('Admin/login');

            }else{

                $this->session->set_flashdata('error', 'Error! Register Again');
                redirect('Admin/login');

            }

        }  
    }

}
